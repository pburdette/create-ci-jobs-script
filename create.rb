#!/usr/bin/env ruby
require 'yaml'

DATA = {}

puts "How many jobs would you like to create?"

num_of_jobs = gets.chomp.to_i

puts "How long do you want each job to run?"

sleep_time = gets.chomp.to_i

num_of_jobs.times do |i|
  DATA.merge!("job_#{i}" => {"stage" => "test", "script" => "sleep #{sleep_time}"})
end

File.open(".gitlab-ci.yml", "w") do |f|
  f.write DATA.to_yaml
end
