# create-ci-jobs-script

This script is used for creating mock jobs for use when developing against the GitLab project. They can be used for performance testing or feature testing etc.

Once you've pulled down the repo, Run `ruby create.rb` in the correct directory.

Enter the number of jobs you want to create

Enter how long you want the job to run

Boom, you now have a `.gitlab-ci.yml` file with those jobs
